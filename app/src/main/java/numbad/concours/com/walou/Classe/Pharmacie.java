package numbad.concours.com.walou.Classe;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by HP on 25/02/2016.
 */
public class Pharmacie extends RealmObject {
    @PrimaryKey
    private int Id;

    public String getAdresse() {
        return Adresse;
    }

    public void setAdresse(String adresse) {
        Adresse = adresse;
    }

    private String Nom;
    private String Adresse;
    private double Latitude;
    private double Longitude;
    private String Telephone;
    private int Etat;


    public int getEtat() {
        return Etat;
    }

    public void setEtat(int etat) {
        this.Etat = etat;
    }


    public String getTelephone() {
        return Telephone;
    }

    public void setTelephone(String telephone) {
        this.Telephone = telephone;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        this.Latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        this.Longitude = longitude;
    }

    public int getId() {
        return Id;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        this.Nom = nom;
    }


    public void setId(int id) {
        this.Id = id;
    }

}

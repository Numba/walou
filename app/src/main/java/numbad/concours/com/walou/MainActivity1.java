package numbad.concours.com.walou;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hp.walou.R;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import numbad.concours.com.walou.FragmentPack.HopitalFragment;
import numbad.concours.com.walou.FragmentPack.PharmacieFragment;
import numbad.concours.com.walou.FragmentPack.UserInfosFragment;

public class MainActivity1 extends AppCompatActivity {
    private int PICK_IMAGE_REQUEST = 1;
    private ImageView imageView;
    private Bitmap bitmap;
    private Uri filePath;
    String TITLES[] = {"Pharmacie","Hôpital","Infos-user"};
    int ICONS[] = new int[]{R.drawable.farmacia,R.drawable.medkit,R.drawable.user};
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    private RecyclerView recyclerView;
    private MyAdapter customAdapter;
    private ArrayList<ITemList> arrayList;
    private android.support.v4.app.FragmentTransaction transaction;
    private LinearLayout layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        recyclerView=(RecyclerView)findViewById(R.id.rcycle);
        //imageView=(ImageView)findViewById(R.id.imageView5);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        arrayList = new ArrayList<ITemList>();
        layout=(LinearLayout)findViewById(R.id.panel);
        for (int i = 0;i<3;i++){
            ITemList listItem = new ITemList();
            listItem.setTitle(TITLES[i]);
            listItem.setImg1(ICONS[i]);
            arrayList.add(listItem);
        }
        customAdapter = new MyAdapter(arrayList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(customAdapter);
        this.drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        this.drawerToggle = new ActionBarDrawerToggle(this,this.drawerLayout,toolbar,R.string.openDrawer,R.string.closeDrawer){
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawerLayout.addDrawerListener(this.drawerToggle);
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //setContentView(R.layout.landscapeview);
            drawerToggle.onConfigurationChanged(newConfig);

        } else {
            //setContentView(R.layout.main1);
            drawerToggle.onConfigurationChanged(newConfig);

        }
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // synchroniser le drawerToggle après la restauration via onRestoreInstanceState
        drawerToggle.syncState();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    public void onSectionAttached(int number) {
        switch (number) {
            case 0:
              //if(!toolbar.getTitle().equals("Pharmacie")) {
                  toolbar.setTitle("Pharmacie");
                  PharmacieFragment ct = new PharmacieFragment();
                  transaction = getSupportFragmentManager().beginTransaction();
                  transaction.replace(R.id.container, ct).commit();
              //}
                break;
            case 1:
              //  if(!toolbar.getTitle().equals("HÔpital")) {
                    toolbar.setTitle("HÔpital");
                    HopitalFragment ht = new HopitalFragment();
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, ht).commit();
                //}
                break;
            case 2:
                //if(!toolbar.getTitle().equals("Infos-user")) {
                    toolbar.setTitle("Infos-user");
                    UserInfosFragment ut = new UserInfosFragment();
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, ut).commit();
                //}
                break;
         }
        drawerLayout.closeDrawers();
    }
    private class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
        public ArrayList<ITemList> getList() {
            return list;
        }
        public void setList(ArrayList<ITemList> list) {
            this.list = list;
        }
        private ArrayList<ITemList> list;
        private Context context;
        public MyAdapter(ArrayList<ITemList> list) {
            this.list = list;
        }
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_recycler, null);
            MyViewHolder viewHolder = new MyViewHolder(view);
            return viewHolder;
        }
        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            holder.title.setText(list.get(position).getTitle());
            holder.img.setImageResource(list.get(position).getImg1());
        }
        @Override
        public int getItemCount() {
            return list.size();
        }
        public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            protected ImageView img;
            protected TextView title;
            protected CardView cv;
            public MyViewHolder(View itemView) {
                super(itemView);
                this.title = (TextView) itemView.findViewById(R.id.textView);
                this.img = (ImageView) itemView.findViewById(R.id.imageView);
                cv=(CardView)itemView.findViewById(R.id.cv);
                itemView.setOnClickListener(this);
            }
            @Override
            public void onClick(View v) {
                    int posi = getAdapterPosition();
                    onSectionAttached(posi);
                    ITemList iTemList = arrayList.get(posi);
                    Toast.makeText(getApplicationContext(), iTemList.getTitle(), Toast.LENGTH_SHORT).show();
                    drawerLayout.closeDrawers();
            }
        }
    }
    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }
}
package numbad.concours.com.walou.FragmentPack;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hp.walou.R;


/**
 * Created by Papis Ndiaye on 25/05/2016.
 */
public class HopitalFragment extends Fragment {
    private Context context;
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       // context = container.getContext();
        View rootView = inflater.inflate(R.layout.hopital_fragment, container, false);
        return rootView;
    }
}

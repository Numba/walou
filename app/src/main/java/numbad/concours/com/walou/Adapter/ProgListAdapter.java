package numbad.concours.com.walou.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import numbad.concours.com.walou.Classe.Pharmacie;
import numbad.concours.com.walou.MapsActivity;
import com.example.hp.walou.R;

import java.util.List;

import io.realm.RealmObject;

/**
 * Created by HP on 25/05/2016.
 */
public class ProgListAdapter extends ArrayAdapter<RealmObject> {
    private int layout;
    private List<RealmObject> mObjects;
    private Context context;
    Pharmacie pharmacie;
    MapsActivity m;
    private LayoutInflater layoutInflater;

    public List<RealmObject> getmObjects() {
        return mObjects;
    }

    public void setmObjects(List<RealmObject> mObjects) {
        this.mObjects = mObjects;
    }

    public Pharmacie getPharmacie() {
        return pharmacie;
    }

    public ProgListAdapter(Context context, List<RealmObject> objects) {
        super(context, 0, objects);
        mObjects = objects;
        this.context = context;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder mainViewholder;
        if (convertView == null) {
            mainViewholder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.list_item, parent, false);
            //  convertView = inflater.inflate(layout, parent, false);
            // ViewHolder viewHolder = new ViewHolder();
            mainViewholder.title = (TextView) convertView.findViewById(R.id.list_item_text);
            mainViewholder.time = (TextView) convertView.findViewById(R.id.list_item_time);



            convertView.setTag(mainViewholder);
        } else {
            mainViewholder = (ViewHolder) convertView.getTag();


        }
        if (position == 0) {
            pharmacie = (Pharmacie) mObjects.get(position);
            //  Toast.makeText(context, "title " + pharmacie.getNom(), Toast.LENGTH_SHORT).show();


        }
        mainViewholder.title.setText(((Pharmacie) (mObjects.get(position))).getNom());
        mainViewholder.time.setText(((Pharmacie) (mObjects.get(position))).getAdresse());
      //  mainViewholder.comment.setText(((Pharmacie) (mObjects.get(position))).getLocalite());

        //   mainViewholder.place.setText(((Pharmacie) (mObjects.get(position))).getPlace());
        // mainViewholder.button.setText("ok");

     /*   mainViewholder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Button was clicked for list item " + position, Toast.LENGTH_SHORT).show();
            }
        });*/
        mainViewholder.title.setText(((Pharmacie) (mObjects.get(position))).getNom());
      //  mainViewholder.time.setText(((Pharmacie) (mObjects.get(position))).getDateDo());
      //  mainViewholder.comment.setText(((Pharmacie) (mObjects.get(position))).getLocalite());
        //  mainViewholder.place.setText(((Pharmacie) (mObjects.get(position))).getPlace());


        return convertView;
    }

    static class ViewHolder {


        TextView title, time, comment, place;

    }

}
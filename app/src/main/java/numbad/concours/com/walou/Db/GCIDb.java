package numbad.concours.com.walou.Db;

import android.content.Context;


import numbad.concours.com.walou.Api.RealmSerializer;
import numbad.concours.com.walou.Api.WalouApi;
import numbad.concours.com.walou.Classe.Pharmacie;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by HP on 08/03/2016.
 */
public class GCIDb  {


    private Realm realm=null;
    private WalouApi walouApi;
    private  Context ctx=null;

    public GCIDb(Context ctx) {
        ctx=ctx;
        this.realm = Realm.getInstance(ctx);
    }


    public ArrayList<RealmObject> getMission() {
        RealmResults<Pharmacie> result = realm.where(Pharmacie.class)
                .findAll();
        ArrayList<RealmObject> pharmacies = new ArrayList<>();
        for(Pharmacie ms:result) pharmacies.add(ms);

        return pharmacies;
    }


    public void addObjects(List<RealmObject> objects) {
        realm.beginTransaction();
        List<RealmObject> temp = realm.copyToRealmOrUpdate(objects);
        if(temp != null) realm.commitTransaction();
    }
    public void getInitializeApi(RealmObject object) {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .registerTypeAdapter(object.getClass(), new RealmSerializer())
                .create();

        GsonConverter converter = new GsonConverter(gson);

        setWalouApi(new RestAdapter.Builder()
                .setEndpoint(WalouApi.walouApi)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(converter)
                .build()
                .create(WalouApi.class));

    }

    public WalouApi getWalouApi() {
        return walouApi;
    }

    public void setWalouApi(WalouApi walouApi) {
        this.walouApi = walouApi;
    }
}

package numbad.concours.com.walou;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hp.walou.R;

import java.util.ArrayList;

/**
 * Created by Papis Ndiaye on 27/02/2016.
 */
public class CustomAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<ListItem> arrayList;

    public CustomAdapter(Context context,ArrayList<ListItem> arrayList)
    {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater vi =
                    (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.ligne_selection, null);
        }
        final ListItem listItem = arrayList.get(position);
        if (listItem != null)
            ((TextView) convertView.findViewById(R.id.the_text)).setText(listItem.getItemName());
            ((ImageView) convertView.findViewById(R.id.imageView1)).setImageResource(listItem.getItemImage());
        return convertView;
    }
}

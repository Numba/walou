package numbad.concours.com.walou.Utils;

import com.google.gson.JsonObject;

import java.lang.reflect.Field;

/**
 * Created by HP on 08/03/2016.
 */

public class Utils {

    public static String toGSON(Object object){
        if(object==null) return "{object: null}";
        final JsonObject jsonObject = new JsonObject();
        Field[] attributes = object.getClass().getDeclaredFields();

        for (Field field : attributes) {
            if(field==null) return "{field: null}";
            field.setAccessible(true);

            try {

                if(field.get(object)==null) return "{object: null}";
                jsonObject.addProperty(field.getName() , field.get(object).toString());

            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }
        return jsonObject.toString();

    }

}

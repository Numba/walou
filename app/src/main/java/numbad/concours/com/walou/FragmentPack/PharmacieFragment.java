package numbad.concours.com.walou.FragmentPack;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hp.walou.R;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import io.realm.RealmObject;
import numbad.concours.com.walou.Adapter.ProgListAdapter;
import numbad.concours.com.walou.Classe.MyMakerData;
import numbad.concours.com.walou.Classe.Pharmacie;
import numbad.concours.com.walou.Db.GCIDb;
import numbad.concours.com.walou.DirectionsJSONParser;
import numbad.concours.com.walou.GPSTracker;
import numbad.concours.com.walou.Utils.PreferencesManager;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by Papis Ndiaye on 25/05/2016.
 */
public class PharmacieFragment extends Fragment {

    ArrayList<MyMakerData> myMakerDatas = new ArrayList<MyMakerData>();
    ExpandableListView expandableListView;
    ArrayList<MarkerOptions> malist = new ArrayList<MarkerOptions>();
    private GoogleMap mMap;
    private MapView mapView;
    ProgListAdapter progListAdapter;
    List<RealmObject> addDb = new ArrayList<>();
    ArrayList<RealmObject> finale = new ArrayList<>();
    ArrayList<Pharmacie> missionse = new ArrayList<>();
    List<Pharmacie> _listDataHeader;
    List<Pharmacie> _listDataHeader1;
    private FragmentActivity myContext;
    HashMap<Pharmacie, List<Pharmacie>> _listDataChild;
    ImageView image;
    private SlidingUpPanelLayout slidingLayout;
    List<String> locations;

    View coordinatorLayoutView;
    final List<RealmObject> items = new ArrayList<>();
    final List<RealmObject> listtim = new ArrayList<>();


    public Context context = this.context;

    public Context getContext() {
        return context;
    }

    final List<RealmObject> listnotime = new ArrayList<>();

    PreferencesManager preferencesManager = new PreferencesManager();
    int idmission;

    boolean clicked = false;
    GCIDb db;
    boolean first = false;

    Location location = new Location("loc");
    GPSTracker gps;
    Fragment f ;

    View rootView1;

    TextView duree, groupper, distances;
    Context cont;
    ListView listview;
    double latitude, latitude1, longitude, longitude1;


    MyMakerData posi, desti;
    class Custom implements Comparator<HashMap<String, String>> {

        @Override
        public int compare(HashMap<String, String> lhs, HashMap<String, String> rhs) {
            int a = 0;
            a = lhs.get("dist").compareTo(rhs.get("dist"));
            return a;
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       // context = container.getContext();
        View rootView = inflater.inflate(R.layout.activity_maps, container, false);
        slidingLayout = (SlidingUpPanelLayout) rootView.findViewById(R.id.sliding_layout);
        groupper = (TextView) rootView.findViewById(R.id.distance);
        _listDataChild = new HashMap<>();
        _listDataHeader = new ArrayList<>();
        _listDataHeader1 = new ArrayList<>();
        //
        cont = this.getActivity();
        mapView = (MapView)rootView.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        //mMap = mapView.getMap();
        rootView1 = rootView;
       /* mapView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action){
                    case MotionEvent.ACTION_DOWN:
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                v.onTouchEvent(event);
                return true;
            }
        });*/
        locations = new ArrayList<>();
//        Collections.sort(recuplist, new CustomComparator());
        image = (ImageView) rootView.findViewById(R.id.imageView);
        image.setImageResource(R.drawable.up);
        slidingLayout.setPanelSlideListener(onSlideListener());
        LayoutInflater factory = LayoutInflater.from(cont);
        // final EditText et = (EditText) alertDialogView.findViewById(R.id.EditText1);

        gps = new GPSTracker(cont);

        LocationManager locationManager = (LocationManager) cont.getSystemService(cont.LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            showSettingsAlert();
          /*  Intent intent1 = new Intent(MapsActivity.this, MapsActivity.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            MapsActivity.this.startActivity(intent1);*/
        } else if (gps.canGetLocation()) {
            //latitude = gps.getLatitude();
            //longitude = gps.getLongitude();
            location = gps.getLocation();
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            preferencesManager.setValue(cont, "context", "" + cont);
        }
        listview = (ListView) rootView.findViewById(R.id.listProg);
        db = new GCIDb(cont);

        progListAdapter = new ProgListAdapter(cont, new ArrayList<RealmObject>());

        db.getInitializeApi(new Pharmacie());
        //getAllItemObject();

        listview.setAdapter(progListAdapter);
        while (listview.getAdapter() == null) {
            Toast.makeText(cont.getApplicationContext(), "liste vide",
                    Toast.LENGTH_SHORT).show();

        }
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });
        latitude1 = preferencesManager.getDouble(cont, "lati");
        longitude1 = preferencesManager.getDouble(cont, "longi");
        String namemision = preferencesManager.getValue(cont, "name");
        idmission = preferencesManager.getValueInt(cont, "idmiss");
        String nameuser = preferencesManager.getValue(cont, "nameuser");






        groupper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<HashMap<String, String>> hashMaps = new ArrayList<>();



                Toast.makeText(cont.getApplicationContext(), "distance click",
                        Toast.LENGTH_SHORT).show();
                for (int i = 0; i < items.size(); i++) {

                    HashMap<String, String> hashMap = new HashMap<>();
                    Log.i("duratio", "" + i);
                    hashMap.put("id", i + "");
                    hashMap.put("dist", "" + CalculationByDistance(new LatLng(latitude, longitude), new LatLng(((Pharmacie) items.get(i)).getLatitude(),
                            ((Pharmacie) items.get(i)).getLatitude())));
                    hashMaps.add(hashMap);

                }
                Collections.sort(hashMaps, new Custom());
                for (int i = 0; i < hashMaps.size(); i++) {
                    Log.i("duration", hashMaps.get(i).get("dist"));
                    finale.add(items.get(Integer.parseInt(hashMaps.get(i).get("id"))));


                }

                listview.setAdapter(null);
                Double recupLat = ((Pharmacie) finale.get(0)).getLatitude();
                Double recupLong = ((Pharmacie) finale.get(0)).getLongitude();
                preferencesManager.setDouble(cont, "lati1", recupLat);
                preferencesManager.setDouble(cont, "longi1", recupLong);
                progListAdapter.setmObjects(finale);
                progListAdapter.notifyDataSetChanged();
                listview.setAdapter(progListAdapter);

            }
        });



        posi = new MyMakerData();
        desti = new MyMakerData();

        posi.setLatLng(new LatLng(latitude, longitude));

        posi.setTitle(nameuser);
        desti.setLatLng(new LatLng(latitude1, longitude1));
        desti.setTitle(namemision);
        desti.setBitmap(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

        myMakerDatas.add(posi);
        myMakerDatas.add(desti);

        for (int i = 0; i < myMakerDatas.size(); i++) {
            MyMakerData mark = myMakerDatas.get(i);
            MarkerOptions marker = new MarkerOptions()
                    .position(mark.getLatLng())
                    .title(mark.getTitle())
                    .icon(mark.getBitmap());
            malist.add(marker);

        }

        if (listview.getAdapter() != null) {
            setUpMapIfNeeded(malist);
        }


        final LatLng position = new LatLng(latitude, longitude);
        final LatLng destination = new LatLng(latitude1, longitude1);
        Circle circle = mMap.addCircle(new CircleOptions()
                .center(new LatLng(latitude1, longitude1))
                .radius(35)
                .strokeColor(Color.WHITE)
                .fillColor(Color.parseColor("#00FFFFFF")));


        Location loc1 = new Location("loc1");
        Location loc2 = new Location("loc2");
        LatLng l1 = new LatLng(latitude, longitude);
        loc1.setLatitude(latitude);
        loc1.setLongitude(longitude);
        loc2.setLatitude(latitude1);
        loc2.setLongitude(longitude1);
        float distance1 = loc1.distanceTo(loc2);

        float[] distance = new float[2];

        Location.distanceBetween(l1.latitude, l1.longitude,
                circle.getCenter().latitude, circle.getCenter().longitude, distance);
       // try {
            MapsInitializer.initialize(this.getActivity());
       // } catch (GooglePlayServicesNotAvailableException e) {
       //     e.printStackTrace();
       // }
        return rootView;
    }
    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
        setUpMapIfNeeded(malist);
    }


    private void setUpMapIfNeeded(ArrayList<MarkerOptions> listmark) {

        if (mMap == null) {
           // FragmentManager fragManager = myContext.getSupportFragmentManager();
          /*  mMap = ((SupportMapFragment) myContext.getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();*/
            mMap = mapView.getMap();
            /*if(mapView!=null)
            {

                mMap = mapView.getMap();

            }*/
            if (mMap != null) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15));
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(true);

                for (int i = 0; i < listmark.size(); i++) {

                    mMap.addMarker(listmark.get(i));

                }

            }
        }
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();

        mapView.onDestroy();
    }
    @Override
    public void onLowMemory()
    {
        super.onLowMemory();

        mapView.onLowMemory();
    }

    private void setUpMap() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {

// Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

// Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

// Sensor enabled
        String sensor = "sensor=false";

// Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

// Output format
        String output = "json";

// Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }


    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

// Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

// Connecting to url
            urlConnection.connect();

// Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {

        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

// For storing data from web service
            String data = "";

            try {
// Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

// Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }


    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

// Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

// Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();

// Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

// Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

// Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(7);
                lineOptions.color(Color.RED);

            }

// Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
        }
    }

    private void getAllItemObject() {

        final Pharmacie pharmacie = new Pharmacie();
        int iduser = preferencesManager.getValueInt(cont, "idUser");
        db.getWalouApi().getpharmacie(new Callback<List<Pharmacie>>() {
            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            @Override
            public void success(List<Pharmacie> pharmacies, Response response) {
                items.addAll(pharmacies);

                db.addObjects(items);

                Log.d("log11", "erreur: " + items.toString());
                Log.d("log11", "erreurddd: " + listtim.size());


                if (first == false) addDb.addAll(db.getMission());
                first = true;
                progListAdapter.addAll(items);


                Double recupLat = ((Pharmacie) items.get(0)).getLatitude();
                Double recupLong = ((Pharmacie) items.get(0)).getLongitude();
                String recupname = ((Pharmacie) items.get(0)).getNom();
                int recupid = ((Pharmacie) items.get(0)).getId();

                preferencesManager.setDouble(cont, "lati", recupLat);
                preferencesManager.setDouble(cont, "longi", recupLong);
                preferencesManager.setValue(cont, "name", recupname);
                preferencesManager.setValueInt(cont, "idmiss", recupid);

            }

            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            @Override
            public void failure(RetrofitError retrofitError) {
                Log.d("log100", "echec45 : " + retrofitError.getResponse());
                //     Toast.makeText(getActivity(), "erreur ",
                //           Toast.LENGTH_SHORT).show();
                if (first == false) addDb.addAll(db.getMission());
                first = true;
                progListAdapter.addAll(addDb);
            }
        });
    }


    private SlidingUpPanelLayout.PanelSlideListener onSlideListener() {
        return new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View view, float v) {
            }

            @Override
            public void onPanelCollapsed(View view) {
                image.setImageResource(com.example.hp.walou.R.drawable.up);
            }

            @Override
            public void onPanelExpanded(View view) {
                image.setImageResource(com.example.hp.walou.R.drawable.down);
            }

            @Override
            public void onPanelAnchored(View view) {

            }

            @Override
            public void onPanelHidden(View view) {

            }
        };
    }

    private void showSettingsAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(cont);
        alertDialogBuilder.setMessage("GPS desactivé.Voulez vouz l'activé ?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


    public double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);

        return Radius * c;
    }
}

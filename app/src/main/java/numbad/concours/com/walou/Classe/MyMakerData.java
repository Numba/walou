package numbad.concours.com.walou.Classe;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by HP on 01/03/2016.
 */
public class MyMakerData {

    private LatLng latLng;
    private String title;
    private BitmapDescriptor bitmap;
    private boolean visible;


    public BitmapDescriptor getBitmap() {
        return bitmap;
    }

    public void setBitmap(BitmapDescriptor bitmap) {
        this.bitmap = bitmap;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}

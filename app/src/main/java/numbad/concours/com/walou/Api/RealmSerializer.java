package numbad.concours.com.walou.Api;

/**
 * Created by HP on 08/03/2016.
 */

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Field;
import java.lang.reflect.Type;

import io.realm.RealmObject;

public class RealmSerializer implements JsonSerializer<RealmObject> {

 @Override
 public JsonElement serialize(RealmObject object, Type typeOfSrc, JsonSerializationContext context) {
  final JsonObject jsonObject = new JsonObject();
  Field[] attributes = object.getClass().getDeclaredFields();
  for (Field field : attributes) {
   field.setAccessible(true);
   try {
    jsonObject.addProperty( field.getName(), field.get(object).toString());
   } catch (IllegalAccessException e) {
    e.printStackTrace();
   }

  }
  return jsonObject;
 }
}

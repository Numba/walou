package numbad.concours.com.walou;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hp.walou.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import io.realm.RealmObject;
import numbad.concours.com.walou.Adapter.ProgListAdapter;
import numbad.concours.com.walou.Classe.MyMakerData;
import numbad.concours.com.walou.Classe.Pharmacie;
import numbad.concours.com.walou.Db.GCIDb;
import numbad.concours.com.walou.Utils.PreferencesManager;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

class Custom implements Comparator<HashMap<String, String>> {

    @Override
    public int compare(HashMap<String, String> lhs, HashMap<String, String> rhs) {
        int a = 0;
        a = lhs.get("dist").compareTo(rhs.get("dist"));
        return a;

    }

}
public class MapsActivity extends FragmentActivity {

    ArrayList<MyMakerData> myMakerDatas = new ArrayList<MyMakerData>();
    ExpandableListView expandableListView;
    ArrayList<MarkerOptions> malist = new ArrayList<MarkerOptions>();
    private GoogleMap mMap;
    ProgListAdapter progListAdapter;
    List<RealmObject> addDb = new ArrayList<>();
    ArrayList<RealmObject> finale = new ArrayList<>();
    ArrayList<Pharmacie> missionse = new ArrayList<>();
    List<Pharmacie> _listDataHeader;
    List<Pharmacie> _listDataHeader1;

    HashMap<Pharmacie, List<Pharmacie>> _listDataChild;
    ImageView image;
    private SlidingUpPanelLayout slidingLayout;
    List<String> locations;

    View coordinatorLayoutView;
    final List<RealmObject> items = new ArrayList<>();
    final List<RealmObject> listtim = new ArrayList<>();


    public Context context = this.context;

    public Context getContext() {
        return context;
    }

    final List<RealmObject> listnotime = new ArrayList<>();

    PreferencesManager preferencesManager = new PreferencesManager();
    int idmission;

    boolean clicked = false;
    GCIDb db;
    boolean first = false;

    Location location = new Location("loc");
    GPSTracker gps;


    TextView duree, groupper, distances;

    ListView listview;
    double latitude, latitude1, longitude, longitude1;


    MyMakerData posi, desti;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        slidingLayout = (SlidingUpPanelLayout) findViewById(com.example.hp.walou.R.id.sliding_layout);
        groupper = (TextView) findViewById(com.example.hp.walou.R.id.distance);
        _listDataChild = new HashMap<>();
        _listDataHeader = new ArrayList<>();
        _listDataHeader1 = new ArrayList<>();
//

                locations = new ArrayList<>();
//        Collections.sort(recuplist, new CustomComparator());
        image = (ImageView) findViewById(com.example.hp.walou.R.id.imageView);
        image.setImageResource(com.example.hp.walou.R.drawable.up);
        slidingLayout.setPanelSlideListener(onSlideListener());
        LayoutInflater factory = LayoutInflater.from(this);
        // final EditText et = (EditText) alertDialogView.findViewById(R.id.EditText1);

        gps = new GPSTracker(MapsActivity.this);


        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            showSettingsAlert();
          /*  Intent intent1 = new Intent(MapsActivity.this, MapsActivity.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            MapsActivity.this.startActivity(intent1);*/
        } else if (gps.canGetLocation()) {

            //latitude = gps.getLatitude();
            //longitude = gps.getLongitude();
            location = gps.getLocation();
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            preferencesManager.setValue(MapsActivity.this, "context", "" + MapsActivity.this);

        }
        listview = (ListView) findViewById(com.example.hp.walou.R.id.listProg);
        db = new GCIDb(this);

        progListAdapter = new ProgListAdapter(this, new ArrayList<RealmObject>());


        db.getInitializeApi(new Pharmacie());
        getAllItemObject();

        listview.setAdapter(progListAdapter);
        while (listview.getAdapter() == null) {
            Toast.makeText(getApplicationContext(), "liste vide",
                    Toast.LENGTH_SHORT).show();

        }
listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    }
});
        latitude1 = preferencesManager.getDouble(MapsActivity.this, "lati");
        longitude1 = preferencesManager.getDouble(MapsActivity.this, "longi");
        String namemision = preferencesManager.getValue(MapsActivity.this, "name");
        idmission = preferencesManager.getValueInt(MapsActivity.this, "idmiss");
        String nameuser = preferencesManager.getValue(MapsActivity.this, "nameuser");






        groupper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<HashMap<String, String>> hashMaps = new ArrayList<>();



                Toast.makeText(getApplicationContext(), "distance click",
                        Toast.LENGTH_SHORT).show();
                for (int i = 0; i < items.size(); i++) {

                    HashMap<String, String> hashMap = new HashMap<>();
                    Log.i("duratio", "" + i);
                    hashMap.put("id", i + "");
                    hashMap.put("dist", "" + CalculationByDistance(new LatLng(latitude, longitude), new LatLng(((Pharmacie) items.get(i)).getLatitude(),
                            ((Pharmacie) items.get(i)).getLatitude())));
                    hashMaps.add(hashMap);

                }
                Collections.sort(hashMaps, new Custom());
                for (int i = 0; i < hashMaps.size(); i++) {
                    Log.i("duration", hashMaps.get(i).get("dist"));
                    finale.add(items.get(Integer.parseInt(hashMaps.get(i).get("id"))));


                }

                listview.setAdapter(null);
                Double recupLat = ((Pharmacie) finale.get(0)).getLatitude();
                Double recupLong = ((Pharmacie) finale.get(0)).getLongitude();
                preferencesManager.setDouble(MapsActivity.this, "lati1", recupLat);
                preferencesManager.setDouble(MapsActivity.this, "longi1", recupLong);
                progListAdapter.setmObjects(finale);
                progListAdapter.notifyDataSetChanged();
                listview.setAdapter(progListAdapter);

            }
        });



        posi = new MyMakerData();
        desti = new MyMakerData();

        posi.setLatLng(new LatLng(latitude, longitude));

        posi.setTitle(nameuser);
        desti.setLatLng(new LatLng(latitude1, longitude1));
        desti.setTitle(namemision);
        desti.setBitmap(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

        myMakerDatas.add(posi);
        myMakerDatas.add(desti);

        for (int i = 0; i < myMakerDatas.size(); i++) {
            MyMakerData mark = myMakerDatas.get(i);
            MarkerOptions marker = new MarkerOptions()
                    .position(mark.getLatLng())
                    .title(mark.getTitle())
                    .icon(mark.getBitmap());
            malist.add(marker);

        }

        if (listview.getAdapter() != null) {
            setUpMapIfNeeded(malist);
        }


        final LatLng position = new LatLng(latitude, longitude);
        final LatLng destination = new LatLng(latitude1, longitude1);
        Circle circle = mMap.addCircle(new CircleOptions()
                .center(new LatLng(latitude1, longitude1))
                .radius(35)
                .strokeColor(Color.WHITE)
                .fillColor(Color.parseColor("#00FFFFFF")));


        Location loc1 = new Location("loc1");
        Location loc2 = new Location("loc2");
        LatLng l1 = new LatLng(latitude, longitude);
        loc1.setLatitude(latitude);
        loc1.setLongitude(longitude);
        loc2.setLatitude(latitude1);
        loc2.setLongitude(longitude1);
        float distance1 = loc1.distanceTo(loc2);

        float[] distance = new float[2];

        Location.distanceBetween(l1.latitude, l1.longitude,
                circle.getCenter().latitude, circle.getCenter().longitude, distance);






    }


    protected void onResume() {
        super.onResume();

        setUpMapIfNeeded(malist);

    }


    private void setUpMapIfNeeded(ArrayList<MarkerOptions> listmark) {

        if (mMap == null) {

            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(com.example.hp.walou.R.id.map))
                    .getMap();

            if (mMap != null) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15));
                mMap.setMyLocationEnabled(true);
                // mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(true);

                for (int i = 0; i < listmark.size(); i++) {

                    mMap.addMarker(listmark.get(i));

                }


            }
        }
    }


    private void setUpMap() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {

// Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

// Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

// Sensor enabled
        String sensor = "sensor=false";

// Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

// Output format
        String output = "json";

// Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }


    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

// Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

// Connecting to url
            urlConnection.connect();

// Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {

        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

// For storing data from web service
            String data = "";

            try {
// Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
// doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

// Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }


    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

// Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

// Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();

// Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

// Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

// Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(7);
                lineOptions.color(Color.RED);

            }

// Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
        }
    }

    private void getAllItemObject() {

        final Pharmacie pharmacie = new Pharmacie();
        int iduser = preferencesManager.getValueInt(MapsActivity.this, "idUser");
        db.getWalouApi().getpharmacie(new Callback<List<Pharmacie>>() {
            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            @Override
            public void success(List<Pharmacie> pharmacies, Response response) {
                items.addAll(pharmacies);

                db.addObjects(items);

                Log.d("log11", "erreur: " + items.toString());
                Log.d("log11", "erreurddd: " + listtim.size());


                if (first == false) addDb.addAll(db.getMission());
                first = true;
                progListAdapter.addAll(items);


                Double recupLat = ((Pharmacie) items.get(0)).getLatitude();
                Double recupLong = ((Pharmacie) items.get(0)).getLongitude();
                String recupname = ((Pharmacie) items.get(0)).getNom();
                int recupid = ((Pharmacie) items.get(0)).getId();

                preferencesManager.setDouble(MapsActivity.this, "lati", recupLat);
                preferencesManager.setDouble(MapsActivity.this, "longi", recupLong);
                preferencesManager.setValue(MapsActivity.this, "name", recupname);
                preferencesManager.setValueInt(MapsActivity.this, "idmiss", recupid);


            }

            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            @Override
            public void failure(RetrofitError retrofitError) {
                Log.d("log100", "echec45 : " + retrofitError.getResponse());
                Toast.makeText(getApplicationContext(), "erreur ",
                        Toast.LENGTH_SHORT).show();
                if (first == false) addDb.addAll(db.getMission());
                first = true;
                progListAdapter.addAll(addDb);
            }
        });


    }


    private SlidingUpPanelLayout.PanelSlideListener onSlideListener() {
        return new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View view, float v) {
            }

            @Override
            public void onPanelCollapsed(View view) {
                image.setImageResource(com.example.hp.walou.R.drawable.up);
            }

            @Override
            public void onPanelExpanded(View view) {
                image.setImageResource(com.example.hp.walou.R.drawable.down);
            }

            @Override
            public void onPanelAnchored(View view) {

            }

            @Override
            public void onPanelHidden(View view) {

            }
        };
    }

    private void showSettingsAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS desactivé.Voulez vouz l'activé ?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


    public double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);

        return Radius * c;
    }
}
